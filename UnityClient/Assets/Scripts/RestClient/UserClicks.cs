﻿using UnityEngine;
using System;

[Serializable]
public class UserClicks
{
    public string userIdent;
    public int clicksCount;

    public override string ToString()
    {
        return JsonUtility.ToJson(this, true);
    }
}
