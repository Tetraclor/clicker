﻿using UnityEngine;
using System;

[Serializable]
public class ResourceCount
{
    public int resourceId;
    public long count;

    public override string ToString()
    {
        return JsonUtility.ToJson(this, true);
    }
}
