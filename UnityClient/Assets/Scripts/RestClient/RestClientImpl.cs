using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;
using Proyecto26;
using UnityEngine.Networking;
using UnityEditor;
using System;
using RSG;


// ����������� �����
public static class TypeOf<T>
{
    /* Important! Should be a readonly variable for best performance */
    public static readonly Type Raw = typeof(T);

    public static readonly string Name = Raw.Name;
    public static readonly bool IsValueType = Raw.IsValueType;
    /* etc. */
}

public class RestClientImpl : MonoBehaviour
{
    public static string UriReferalClicks = "/account/myreferals";
    public static string UriReferalKey = "/account/referalkey";
    public static string UriUserGameInfo = "/clickcounter";

    public static Dictionary<string, string> ClassNameToApiUri = new Dictionary<string, string>()
    {
        [TypeOf<UserClicks>.Name] = UriReferalClicks,
        [TypeOf<UserGameInfo>.Name] = UriUserGameInfo
    };

    public static string Token { get; private set; }

    public string API_URL = "";
    public bool UseLocalServer = false;
    public bool DissableCheckSSLCertificate = true;

    private Func<CertificateHandler> CreateCertificateHandler;

    void Start()
    {
        if(UseLocalServer)
        {
            API_URL = "https://localhost:44338";
        }

        if (DissableCheckSSLCertificate)
            CreateCertificateHandler = () => new AllwaysTrueCertificateHandler();
        else
            CreateCertificateHandler = () => new NormalCertificateHandler();

        LogIn("test", "test",
            m => {

            }
            );
    }

    public void Get(Action<string> response = null, string uri = null, Action<string> error = null)
    {
        Request(
             uri,
             response: response,
             error: error
             );
    }

    public void Get<T>(Action<T> response = null, string uri = null, Action<string> error = null)
    {
        uri ??= ClassNameToApiUri[TypeOf<T>.Name];

        Request(
            uri, 
            response: v => response(JsonUtility.FromJson<T>(v)),
            error: error
            );
    }

    public void AddClick(int count, Action<string> response, Action<string> error = null)
    {
        Request(
            $"/clickcounter/add/{count}",
            response: response,
            error: error);
    }

    public void LogIn(string login, string password, Action<string> response, Action<string> error = null)
    {
        Request(
            "/login",
            new Dictionary<string, string>()
            {
                ["login"] = login,
                ["password"] = password,
            },
            v => { SetToken(v); response(v); },
            error
            );
    }

    public void Register(string login, string password, string referalKey, Action<string> response, Action<string> error = null)
    {
        Request(
            "/register",
            new Dictionary<string, string>()
            {
                ["login"] = login,
                ["password"] = password,
                ["referal_key"] = referalKey
            },
            v => { SetToken(v); response(v); },
            error
            );
    }

    public void SetToken(string token)
    {
        Token = token;
        RestClient.DefaultRequestParams["token"] = token;
    }

    public void Request(string api, Dictionary<string, string> param = null, Action<string> response = null, Action<string> error = null)
    {
        var promise = RestClient.Get(new RequestHelper()
        {
            Uri = API_URL + api,
            Params = param,
            CertificateHandler = CreateCertificateHandler()
        });
        HandleServerAnswer(promise, response, error);
    }

    public void Post<T>(T data, string uri = null, Dictionary<string, string> param = null, Action<string> response = null, Action<string> error = null)
    {
        uri ??= ClassNameToApiUri[TypeOf<T>.Name];

        var promise = RestClient.Post(new RequestHelper()
        {
            Uri = API_URL + uri,
            Params = param,
            CertificateHandler = CreateCertificateHandler(),
            Body = data
        });

        HandleServerAnswer(promise, response, error);
    }

    public void HandleServerAnswer(IPromise<ResponseHelper> promise, Action<string> response = null, Action<string> error = null)
    {
        error ??= ((v) => Debug.LogError(v));
        response ??= v => Debug.Log(v);

        promise
            .Then(res =>
        {
            if (res.StatusCode == 200)
                response(res.Text);
            else
                error(res.Error + " " + res.Text);
        })
            .Catch(exp =>
        {
            if (exp is RequestException requestExp)
                if (requestExp.IsNetworkError)
                    error($"Network Error: {requestExp.GetBaseException().Message}");
                else if (requestExp.IsHttpError)
                    error($"HTTP Error: {requestExp.GetBaseException().Message}");
                else
                    error(requestExp.Response);
            else
                error(exp.Message);
        });
    }

    public class NormalCertificateHandler : CertificateHandler
    {

    }

    public class AllwaysTrueCertificateHandler : CertificateHandler 
        // ���� ������� �����, ����� ���������� ���������������� ����������. ������� �� �������� reg.ru ������ �������� �� ��������� �� ����� ��� ���������� �����������.
        // ����� ���-������ ������� ���������� ����������?
    {
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            return true;
        }
    }
}
