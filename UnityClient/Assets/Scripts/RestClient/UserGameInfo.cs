﻿using UnityEngine;
using System;

[Serializable]
public class UserGameInfo
{
    public int countClicks;
    public ResourceCount[] mineResources = new ResourceCount[] { };

    public override string ToString()
    {
        return JsonUtility.ToJson(this, true);
    }

    public UserGameInfo Clone()
    {
        return new UserGameInfo()
        {
            countClicks = countClicks,
            mineResources = (ResourceCount[])mineResources.Clone()
        };
    }
}
