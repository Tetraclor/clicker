using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Clicker.Craft
{ 
    public class CraftItem : ItemSlot, IPointerClickHandler
    {
        private CraftController _craftController;
        [Inject]
        public void Constractor(CraftController craftController)
        {
            _craftController = craftController;
        }
        public void OnPointerClick(PointerEventData eventData)
        {
            if (Item is CraftRecipe)
                Debug.Log("OK");
            _craftController.ViewCraftObject(Item as CraftRecipe);
        }        
    }
}
