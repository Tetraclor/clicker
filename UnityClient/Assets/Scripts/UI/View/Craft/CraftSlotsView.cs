using Clicker.Craft;
using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Clicker.UIView
{
    public class CraftSlotsView : SlotsView
    {
        [SerializeField]
        List<CraftRecipe> _craftRecipes;
        protected override void AssignItems()
        {
            _items = new List<IItem>();
            _items = _craftRecipes.Select(x => x as IItem).ToList();
        }
    }
}
