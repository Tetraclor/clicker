using Clicker.InventorySystem;
using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Clicker.UIView
{
    public class SlotsView : MonoBehaviour
    {
        private int _index = 0;
        protected List<IItem> _items;
        private List<ItemSlot> _slots;
        private List<Page> _pages;
        [SerializeField] private Button _next;
        [SerializeField] private Button _prev;
        [SerializeField] private TextMeshProUGUI _pageNumber;
        private void Awake()
        {
            _next.onClick.AddListener(NextPage);
            _prev.onClick.AddListener(PrevPage);
            _slots = GetComponentsInChildren<ItemSlot>().ToList();
        }        
        protected virtual void AssignItems()
        {
            _items = new List<IItem>();
        }
        public void InitializeWindow()
        {           
            AssignItems();
            CreatePages();
            UpdateWindow();
        }
        private void UpdateButtonsVisibleState()
        {
            if(_index == 0 && _pages.Count == 1)
            {
                _prev.gameObject.SetActive(false);
                _next.gameObject.SetActive(false);
            }
            else if(_index == 0)
            {
                _prev.gameObject.SetActive(false);
                _next.gameObject.SetActive(true);
            }
            else if(_index == _pages.Count -1)
            {
                _prev.gameObject.SetActive(true);
                _next.gameObject.SetActive(false);
            }
            else
            {
                _prev.gameObject.SetActive(true);
                _next.gameObject.SetActive(true);
            }                
        }
        private void UpdatePageNumber()
        {
            _pageNumber.text = string.Format($"{_index + 1}/{_pages.Count}");
        }
        private void NextPage()
        {
            _pages[_index].HideItems();
            _index++;
            UpdateWindow();
        }
        private void PrevPage()
        {
            _pages[_index].HideItems();
            _index--;
            UpdateWindow();
        }
        private void UpdateWindow()
        {
            _pages[_index].ShowItmes();
            UpdateButtonsVisibleState();
            UpdatePageNumber();
        }
        private void CreatePages()
        {
            _pages = new List<Page>();
            var countOfPages = GetCountOfPages();
            for (int pageIndex = 0; pageIndex < countOfPages; pageIndex++)
            {
                CreatePage(pageIndex);
            }
        }
        private int GetCountOfPages()
        {
            int countOfPages = 0;
            if (_items.Count > 0)
                countOfPages = Mathf.CeilToInt((float)_items.Count / _slots.Count);          
            return countOfPages;
        }
        private void CreatePage(int pageIndex)
        {
            var pageItems = new List<IItem>();
            var index = pageIndex * _slots.Count;
            var threshold = index + _slots.Count < _items.Count ? index + _slots.Count : _items.Count;            
            for (int itemIndex = index; itemIndex < index + _slots.Count; itemIndex++)
            {                

                if(itemIndex >= threshold)
                {
                    pageItems.Add(Storage.EmptyItem);
                }
                else
                {
                    pageItems.Add(_items[itemIndex]);
                }
            }          
            _pages.Add(new Page(pageItems, _slots));
        }
    }
}
