using Clicker.InventorySystem;
using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Clicker.UIView
{
    public class InventorySlotsView : SlotsView
    {
        [SerializeField] Inventory _inventory;

        protected override void AssignItems()
        {
            _items = new List<IItem>();
            _items = _inventory.Items.Values.Select(x=>x as IItem).ToList();
        }
    }
}
