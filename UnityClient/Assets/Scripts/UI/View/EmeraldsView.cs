using Clicker.InventorySystem;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EmeraldsView : MonoBehaviour
{
    [SerializeField] 
    Inventory inventory;
    [SerializeField]
    TextMeshProUGUI _text;
    public void OnEmeraldsCountChanged(int value)
    {
        _text.text = value.ToString();
    }
    private void Start()
    {
        OnEmeraldsCountChanged(inventory.Emeralds);
        Inventory.ChangeEmeraldsValueEvent += OnEmeraldsCountChanged;
    }
    private void OnDestroy()
    {
        Inventory.ChangeEmeraldsValueEvent -= OnEmeraldsCountChanged;
    }
}
