using Clicker.InventorySystem;
using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EquipmentSlot : ItemSlot, IPointerClickHandler
{
    public Inventory inventory;
    public void OnPointerClick(PointerEventData eventData)
    {
        inventory.Equipment.TakeOffInstrument();
        UpdateSlot(Storage.EmptyItem);
    }
}
