using Clicker.Items;
using Clicker.Shop;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Clicker.UIView
{
    public class ShopSlot : ItemSlot,IPointerClickHandler
    {
        [Inject]
        SellBuyController sellBuyController;
        public override void OnEndDragAction(PointerEventData pointerEventData)
        {
            base.OnEndDragAction(pointerEventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            sellBuyController.SetItemForBuy(Item);
        }
    }
}
