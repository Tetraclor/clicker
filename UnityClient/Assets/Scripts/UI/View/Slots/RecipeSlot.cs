using Clicker.Craft;
using Clicker.InventorySystem;
using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class RecipeSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
   
    [SerializeField]
    private Image _slotImage;
    [SerializeField]
    private TextMeshProUGUI _countIndicator;
    [SerializeField]
    private SimpleTooltip _tooltip;

    [Header("Hovered parameters")]
    [SerializeField]
    private GameObject _hovered;
    [SerializeField]
    private GameObject _unhovered;

    private IItem _item;
   

    public IItem Item { get => _item; set => _item = value; }
    public void UpdateSlot(IItem item)
    {
        _item = item;
        UpdateSlotInfo();
        UpdateTooltipInfo();
        _tooltip.enabled = item is Empty ? false : true;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        _hovered.SetActive(true);
        _unhovered.SetActive(false);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _hovered.SetActive(false);
        _unhovered.SetActive(true);
    }
    private void UpdateSlotInfo()
    {
        if (_item is ICountable)
            _countIndicator.text = (_item as ICountable).Count.ToString();
        _slotImage.sprite = _item.Sprite;
    }
    private void UpdateTooltipInfo()
    {
        _tooltip.infoRight = _countIndicator.text;
        _tooltip.infoLeft = _item.Name;
    }   
    
}
