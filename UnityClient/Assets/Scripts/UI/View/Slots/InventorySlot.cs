using Clicker.InventorySystem;
using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class InventorySlot : ItemSlot, IPointerClickHandler
{
    public Inventory inventory;
    [Inject]
    public EquipmentSlot equipmentSlot;
    public void OnPointerClick(PointerEventData eventData)
    {        
        equipmentSlot.UpdateSlot(Item);       
    }
}
