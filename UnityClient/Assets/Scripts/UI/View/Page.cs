using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.UIView
{
    public class Page
    {
        private List<IItem> _items;
        private List<ItemSlot> _slots;
        public Page(List<IItem> items, List<ItemSlot> slots)
        {
            _items = items;
            _slots = slots;
            
            CheckItemsCount();
        }

        private void CheckItemsCount()
        {
            if (_items.Count > _slots.Count)
                throw new System.Exception("Items count more than slots");
        }
        public void UpdateItems(List<IItem> items)
        {
            _items = items;
            CheckItemsCount();
        }
        public void ShowItmes()
        {
            for (int i = 0; i < _items.Count; i++)
            {
                _slots[i].UpdateSlot(_items[i]);
            }
            
        }
       
        public void HideItems()
        {
            
        }

    }
}
