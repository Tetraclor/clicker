using Clicker.InventorySystem;
using Clicker.Items;
using Clicker.SaveAndLoad;
using Clicker.UIView;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Clicker.Shop
{
    public class SellBuyController : MonoBehaviour
    {
        [SerializeField]
        private Inventory _inventory;
        public Button BuyButton;
        public Button SellButton;
        public GameObject EmeraldsCost;
        public TextMeshProUGUI DealSum;
        public ItemSlot ItemSlot;

        public InventorySlotsView _inventorySlotsView;
       
        public SaveAndLoadSystem saveAndLoadSystem;

        public IItem _forBuy;
        public IItem _forSell;
        public void SetItemForBuy(IItem item)
        {
            BuyButton.gameObject.SetActive(true);
            SellButton.gameObject.SetActive(false);
            DealSum.text = item.Cost.ToString();
            if(_inventory.Emeralds < item.Cost)
            {
                BuyButton.enabled = false;
            }
            else
                BuyButton.enabled = true;
            _forBuy = item;
            EmeraldsCost.SetActive(true);
            ItemSlot.UpdateSlot(item);
        }
        public void SetItemForSell(IItem item)
        {
            BuyButton.gameObject.SetActive(false);
            SellButton.gameObject.SetActive(true);
            DealSum.text = (item.Cost * (item as ICountable).Count).ToString();
            _forSell = item;
            EmeraldsCost.SetActive(true);
            ItemSlot.UpdateSlot(item);
        }
        public void Buy()
        {
            _inventory.AddItem(_forBuy);
            _inventory.Emeralds -= _forBuy.Cost;
            _forBuy = null;
            SellButton.gameObject.SetActive(false);
            _inventorySlotsView.InitializeWindow();
            EmeraldsCost.SetActive(false);
            ItemSlot.UpdateSlot(Storage.EmptyItem);
            saveAndLoadSystem.SaveInventoryData();
        }
        public void Sell()
        {
            _inventory.Emeralds += _forSell.Cost * (_forSell as ICountable).Count;
            _inventory.RemoveAllItemsOfCertanType(_forSell);
            _forSell = null;
            BuyButton.gameObject.SetActive(false);
            _inventorySlotsView.InitializeWindow();
            EmeraldsCost.SetActive(false);
            ItemSlot.UpdateSlot(Storage.EmptyItem);
            saveAndLoadSystem.SaveInventoryData();
        }
        public void ResetWindow()
        {
            BuyButton.gameObject.SetActive(false);
            SellButton.gameObject.SetActive(false);
            ItemSlot.UpdateSlot(Storage.EmptyItem);
            EmeraldsCost.SetActive(false);
        }
    }
}
