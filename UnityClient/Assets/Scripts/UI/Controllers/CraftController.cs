using Clicker.InventorySystem;
using Clicker.SaveAndLoad;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Clicker.Craft
{
    public class CraftController : MonoBehaviour
    {
        [Inject]
        private SaveAndLoadSystem _saveAndLoadSystem { get; set; }
        [SerializeField]
        Inventory _inventory;
        [SerializeField]
        private RecipeSlot _target;
        [SerializeField]
        private List<RecipeSlot> _recipeSlots;
        [SerializeField]
        private Button _button;
        CraftRecipe _craftRecipe;
        private void Start()
        {
            HideRecipeSlots();
        }
        public void ViewCraftObject(CraftRecipe craftRecipe)
        {
            HideRecipeSlots();
            _craftRecipe = craftRecipe;
            _target.UpdateSlot(craftRecipe._targetItem);
            for(int i = 0; i < craftRecipe._pieces.Count; i++)
            {
                _recipeSlots[i].gameObject.SetActive(true);
                _recipeSlots[i].UpdateSlot(craftRecipe._pieces[i]);
            }
            _button.enabled = CanCraft();
        }
        public bool CanCraft()
        {
            
            bool can = true;
            if (_craftRecipe != null)
            {
                foreach (var piece in _craftRecipe._pieces)
                {
                    InventoryItem item;
                    if (_inventory.Items.TryGetValue(piece.Id, out item))
                    {
                        if (item.Count < piece.count)
                        {
                            can = false;
                            break;
                        }
                    }
                    else
                    {
                        can = false;
                        break;
                    }
                }
            }
            else 
                can = false;
            return can;
        }
        public void ResetTargetObject()
        {
            _target.UpdateSlot(Storage.EmptyItem);
            _craftRecipe = null;
            HideRecipeSlots();
        }
        public void Craft()
        {
            foreach (var piece in _craftRecipe._pieces)
            {
                _inventory.RemoveItem(piece, piece.count);
            }
            _inventory.AddItem(_craftRecipe._targetItem);
            _saveAndLoadSystem.SaveInventoryData();
        }
        public void HideRecipeSlots()
        {
            foreach (var slot in _recipeSlots)
            {
                slot.gameObject.SetActive(false);
            }
        }
    }
}
