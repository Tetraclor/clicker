using Clicker.Craft;
using Clicker.Shop;
using Clicker.UIView;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGUIController : MonoBehaviour
{
    [SerializeField]
    private GameObject _navigationButtons;
    [Space]
    [SerializeField]
    private GameObject _inventoryWindow;
    [SerializeField]
    private GameObject _craftWindow;
    [SerializeField]
    private GameObject _shopWindow;
    [Space]
    [Header("Craft")]
    [SerializeField]
    private CraftController _craftController;    
    [SerializeField]
    private CraftSlotsView _craftSlotsView;
    [Header("Inventory")]
    [SerializeField]
    private InventorySlotsView _inventorySlotsView;
    [Header("Shop")]
    [SerializeField]
    private ShopSlotsView _shopSlotsView;
    [SerializeField]
    private InventorySlotsView _inventoryCraftSlotsView;
    [SerializeField]
    private SellBuyController _sellBuyController;
    public void OpenInventory()
    {
        _inventoryWindow.SetActive(true);
        _navigationButtons.SetActive(false);
        _inventorySlotsView.InitializeWindow();
    }
    public void CloseInventory()
    {
        _inventoryWindow.SetActive(false);
        _navigationButtons.SetActive(true);
    }
    public void OpenShop()
    {
        _shopWindow.SetActive(true);       
        _navigationButtons.SetActive(false);
        _shopSlotsView.InitializeWindow();
        _inventoryCraftSlotsView.InitializeWindow();
    }
    public void CloseShop()
    {       
        _shopWindow.SetActive(false);
        _navigationButtons.SetActive(true);
        _sellBuyController.ResetWindow();
    }
    public void OpenCraft()
    {        
        _craftWindow.SetActive(true);
        _navigationButtons.SetActive(false);        
        _craftSlotsView.InitializeWindow();
    }
    public void CloseCraft()
    {
        _craftController.ResetTargetObject();
        _craftWindow.SetActive(false);
        _navigationButtons.SetActive(true);
    }

}
