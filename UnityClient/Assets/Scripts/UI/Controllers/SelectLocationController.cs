using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.SelectLocation
{
    public class SelectLocationController : MonoBehaviour
    {
        [SerializeField]  private LocationChanger _locationChanger;
        [Space]
        [SerializeField] private GameObject ForestLocationWindow;
        [SerializeField] private GameObject CaveLocationWindow;

        [SerializeField] private GameObject _locationButtons;


        public GameObject SelectWindow;
        private void Awake()
        {
            LocationInSelectWindow.LocationSelectEvent += OnLocationEnter;
        }
        public void NextLocation() =>_locationChanger.NextLocation();
        public void PrevLocation() => _locationChanger.PrevLocation();
        public void OnLocationEnter()
        {
            ForestLocationWindow.SetActive(false);
            CaveLocationWindow.SetActive(false);
            _locationButtons.SetActive(true);
        }
        public void ToLocationSelect()
        {
            ForestLocationWindow.SetActive(false);
            CaveLocationWindow.SetActive(false);
            _locationButtons.SetActive(false);
            SelectWindow.SetActive(true);
        }
        private void OnDestroy()
        {
            LocationInSelectWindow.LocationSelectEvent -= OnLocationEnter;
        }
    }
}
