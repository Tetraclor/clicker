using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Clicker.SelectLocation
{
    public class LocationChanger : MonoBehaviour
    {
        [SerializeField] GameObject _Panel;
        [SerializeField] LocationInSelectWindow _forest;
        [SerializeField] LocationInSelectWindow _cave;
        [SerializeField] private Button _buttonNext;
        [SerializeField] private Button _buttonPrev;
  
        private void Awake()
        {
            SetupInitState();
            LocationInSelectWindow.LocationSelectEvent += OnLocationSelected;            
            _buttonNext.onClick.AddListener(NextLocation);
            _buttonPrev.onClick.AddListener(PrevLocation);
        }
        public void SetupInitState()
        {
            _forest.gameObject.SetActive(true);
            _buttonNext.gameObject.SetActive(true);
            _cave.gameObject.SetActive(false);
            _buttonPrev.gameObject.SetActive(false);
        }
        public void NextLocation()
        {
            _forest.gameObject.SetActive(false);
            _buttonNext.gameObject.SetActive(false);
            _cave.gameObject.SetActive(true);
            _buttonPrev.gameObject.SetActive(true);
        }
        public void PrevLocation()
        {
            SetupInitState();
        }
        public void SetActivePanel(bool flag)
        {
            SetupInitState();
            _Panel.SetActive(flag);
        }
       
        public void OnLocationSelected()
        {
            SetActivePanel(false);
        }
        private void OnDestroy()
        {
            LocationInSelectWindow.LocationSelectEvent -= OnLocationSelected;          
            _buttonNext.onClick.RemoveAllListeners();
            _buttonPrev.onClick.RemoveAllListeners();
        }
    }
}