using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Clicker.SelectLocation
{
    public class LocationInSelectWindow:MonoBehaviour
    {
        [SerializeField] private GameObject _activatedLocation;
        private Button _button;
        public static event Action LocationSelectEvent;
        private void Awake()
        {
            _button = GetComponentInChildren<Button>();
            _button.onClick.AddListener(SelectLocation);
        }
        public void SelectLocation()
        {           
            LocationSelectEvent?.Invoke();
            _activatedLocation.SetActive(true);
        }
        private void OnDestroy()
        {
            _button.onClick.RemoveAllListeners();
        }
    }
}
