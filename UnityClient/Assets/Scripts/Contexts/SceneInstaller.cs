using Clicker.Craft;
using Clicker.SaveAndLoad;
using Clicker.Shop;
using UnityEngine;
using Zenject;

public class SceneInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<CraftController>().FromComponentInHierarchy(true).AsSingle();
        Container.Bind<SaveAndLoadSystem>().FromComponentInHierarchy(true).AsSingle();
        Container.Bind<SellBuyController>().FromComponentInHierarchy(true).AsSingle();
        Container.Bind<EquipmentSlot>().FromComponentInHierarchy(true).AsSingle();
    }
}