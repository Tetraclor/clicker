using Clicker.Notification;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class forgotPasswordButtonScript : MonoBehaviour
{
    private int _buttonClickCount;// ������ ������ � �������� �� �����
    private NotificationPanel _notificationPanel;

    [SerializeField]
    private GameObject _forgotButton;

    [SerializeField]
    private TextMeshProUGUI _textField;
    [SerializeField]
    private Image _buttonBackground;
    // Start is called before the first frame update
    void Awake()
    {
        _buttonClickCount = 0; 
        SetupComponents();
    }

    void Start()
    {
        _notificationPanel = NotificationPanel.Instance;
    }

    void SetupComponents()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick(GameObject forgotButton)
    {
        _notificationPanel.ShowMessage("Send email to us and we will help you");
    }

    private void Disolve()
    {
        StartCoroutine(Disappear());
    }
    private IEnumerator Disappear()
    {
        float time = 0;
        while (time < 2f)
        {
            time += Time.deltaTime;

            float capacity = Mathf.Lerp(1f, 0f, time / 2f);

            var textColor = _textField.color;
            var backgroundColor = _buttonBackground.color;
            textColor.a = backgroundColor.a = capacity;

            _textField.color = textColor;
            _buttonBackground.color = backgroundColor;

            yield return null;
        }
        _forgotButton.SetActive(false);
    }
}
