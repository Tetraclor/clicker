namespace Clicker
{
    public static class Project
    {
        public static string SaveInventoryFile => "Player/InventoryItems";
        public static string Emeralds => "Player/Emeralds";
        public static string InventoryScriptableObject => "Player/Inventory";
        public static string StorageScriptableObject => "Player/Storage";

        public static string GameObjects => "ObjectsInGame/Items";
        public static string Instuments => "ObjectsInGame/Instrument";
        public static class SpecialObject
        {
            public static string Hand => "SpecialObjects/Hand";
            public static string Empty => "SpecialObjects/Empty";
        }
    }
}
