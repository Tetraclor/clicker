using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Clicker.Notification
{
    public class NotificationPanel : MonoBehaviour
    {
        static private NotificationPanel _init;
        public static NotificationPanel Instance { get => _init; }

        [SerializeField]
        private NotificationProperties _properties;

        private TextMeshProUGUI _textField;

        private RectTransform _panelTransform;        
        private Image _panelBackGround;

        private Vector2 NonActivePanelPosition;
        private Vector2 ActivePanelPosition;

        private void Awake()
        {
            _init = this;
            SetupComponents();
        }

        private void SetupComponents()
        {
            _textField = GetComponentInChildren<TextMeshProUGUI>();
            _panelBackGround = GetComponent<Image>();
            _panelTransform = GetComponent<RectTransform>();

            NonActivePanelPosition = _panelTransform.anchoredPosition;
            ActivePanelPosition = NonActivePanelPosition + Vector2.down * _properties.ShiftDistance;
        }
        public void ShowMessage(string message)
        {
            _textField.text = message;
            StopAllCoroutines();
            StartCoroutine(Appear());
        }

        private IEnumerator Appear()
        {
            float time = 0;
            while(time < _properties.AppearTime)
            {
                time += Time.deltaTime;
                TransformColorByTime(time, _properties.AppearTime, 0f, 1f);
                TransformPostitionByTime(time, _properties.AppearTime, NonActivePanelPosition, ActivePanelPosition);
                yield return null;
            }
            StartCoroutine(Display());           
        }
        private IEnumerator Display()
        {        
            yield return new WaitForSeconds(_properties.DisplayTime); 
            StartCoroutine(Disappear());            
        }
        private IEnumerator Disappear()
        {           
            float time = 0;
            while (time < _properties.DisapperTime)
            {
                time += Time.deltaTime;
                TransformColorByTime(time, _properties.DisapperTime, 1f, 0f);
                TransformPostitionByTime(time, _properties.DisapperTime, ActivePanelPosition, NonActivePanelPosition);
                yield return null;
            }
        }

        private void TransformColorByTime(float currentTime, float pointedTime, float startValue, float endValue)
        {
            var textColor = _textField.color;
            var panelColor = _panelBackGround.color;

            textColor.a = panelColor.a = Mathf.Lerp(startValue, endValue, currentTime / pointedTime);

            _textField.color = textColor;
            _panelBackGround.color = panelColor;
        }

        private void TransformPostitionByTime(float currentTime, float pointedTime, Vector3 startPosition, Vector3 endPosition)
        {
            _panelTransform.anchoredPosition = Vector2.Lerp(startPosition, endPosition, currentTime / pointedTime);
        }
    }

    [Serializable]
    public class NotificationProperties
    {
        public float AppearTime;
        public float DisapperTime;
        public float DisplayTime;
        [Space]
        public float ShiftDistance;   
    }

}


