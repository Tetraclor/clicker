using Clicker.InventorySystem;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Clicker.Items
{
    public class ItemSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler,IEndDragHandler, IDragHandler
    {
        [SerializeField]
        private Image _slotImage;
        [SerializeField]
        private TextMeshProUGUI _countIndicator;
        [SerializeField]
        private SimpleTooltip _tooltip;

        [Header("Hovered parameters")]
        [SerializeField]
        private GameObject _hovered;
        [SerializeField]
        private GameObject _unhovered;

        private IItem _item;


        private GameObject _draggingIcon;
        private RectTransform _draggingIconTransform;

        private RectTransform _draggedPanel;       

        public IItem Item { get => _item; set => _item = value; }
        public void UpdateSlot(IItem item)
        {
            _item = item;
            UpdateSlotInfo();
            UpdateTooltipInfo();
            _tooltip.enabled = item is Empty ? false : true;           
        }       
        public void OnPointerEnter(PointerEventData eventData)
        {
            _hovered.SetActive(true);
            _unhovered.SetActive(false);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _hovered.SetActive(false);
            _unhovered.SetActive(true);
        }
        private void UpdateSlotInfo()
        {
            if(_item is ICountable)
                _countIndicator.text = (_item as ICountable).Count.ToString();
            if(_item is Empty)
                _countIndicator.text = string.Empty;
            _slotImage.sprite = _item.Sprite;
        }
        private void UpdateTooltipInfo()
        {
            _tooltip.infoRight = _countIndicator.text;
            _tooltip.infoLeft = _item.Name;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (_item != Storage.EmptyItem)
            {
                CreateIcon();
                _slotImage.sprite = Storage.EmptyItem.Sprite;
                if (_draggingIcon != null)
                    SetDraggedPosition(eventData);
            }
        }
        private void CreateIcon()
        {
            var canvas = new GameObject("IconCanvas").AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            canvas.sortingOrder = 100;
            var scale = canvas.gameObject.AddComponent<CanvasScaler>();
            scale.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            scale.referenceResolution = new Vector2(800, 600);
            _draggedPanel = canvas.GetComponent<RectTransform>();

            _draggingIcon = new GameObject("icon");
            var image = _draggingIcon.AddComponent<Image>();
            _draggingIconTransform = _draggingIcon.GetComponent<RectTransform>();
            _draggingIcon.transform.SetParent(canvas.transform, false);
            _draggingIcon.transform.SetAsLastSibling();
            image.sprite = _slotImage.sprite;

        }

        public void OnEndDrag(PointerEventData eventData)
        {
            OnEndDragAction(eventData);
            _slotImage.sprite = _item.Sprite;
            
            Destroy(_draggedPanel.gameObject);
            Destroy(_draggingIcon);
        }
        public virtual void OnEndDragAction(PointerEventData pointerEventData)
        {

        }
        public void OnDrag(PointerEventData eventData)
        {
            if (_draggingIcon != null)
                SetDraggedPosition(eventData);
        }
        private void SetDraggedPosition(PointerEventData data)
        {            
            Vector3 globalMousePos;
            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(_draggedPanel, data.position, data.pressEventCamera, out globalMousePos))
            {
                _draggingIconTransform.position = globalMousePos;
                _draggingIconTransform.rotation = _draggingIconTransform.rotation;
            }
        }
    }
}
