using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "PickaxeableItem", menuName = "Items/PickaxeableItem")]
    public class PickaxeableItem : ScriptableItem
    {
        public override ItemType Type => ItemType.Pickaxeable;
    }
}