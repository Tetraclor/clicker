﻿using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "AxeableItem", menuName = "Items/AxeableItem")]
    public class AxeableItem : ScriptableItem
    {
        public override ItemType Type => ItemType.Everything;
    }
}