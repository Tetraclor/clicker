using UnityEngine;
namespace Clicker.Items
{
    public abstract class ScriptableItem : ScriptableObject, IItem
    {
        [SerializeField]
        private int _id;
        [Space]        
        [SerializeField]
        private Sprite _sprite;
        [Space]
        [SerializeField]
        private string _name;
        [SerializeField]
        private Tier _tier;

        [SerializeField]
        private int _cost;

        [SerializeField]
        [Multiline]
        private string _description;

        public Sprite Sprite => _sprite;
        public string Name => _name;
        public string Description => _description;
        public Tier Tier => _tier;
        public int Id => _id;
        public int Cost => _cost;

        public abstract ItemType Type { get; }
    }
}