﻿using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "Picaxe", menuName = "Instruments/Picaxe")]
    public class Picaxe : Instrument
    {
        public override ItemType ObtainedType => ItemType.Pickaxeable;
        public override ItemType Type => ItemType.Instrument;
    }
}
