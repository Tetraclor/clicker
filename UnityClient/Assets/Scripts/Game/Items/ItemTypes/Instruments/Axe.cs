﻿using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "Axe", menuName = "Instruments/Axe")]
    public class Axe : Instrument
    {
        public override ItemType ObtainedType => ItemType.Axeable;
        public override ItemType Type => ItemType.Instrument;
    }
}
