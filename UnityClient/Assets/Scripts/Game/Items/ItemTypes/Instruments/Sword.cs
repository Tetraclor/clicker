﻿using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "Sword", menuName = "Instruments/Sword")]
    public class Sword : Instrument
    {
        public override ItemType ObtainedType => ItemType.Swordable;
        public override ItemType Type => ItemType.Instrument;
    }
}
