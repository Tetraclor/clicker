﻿using Clicker.InventorySystem;
using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "Hand", menuName = "Instruments/Hand")]
    public class Hand : ScriptableItem,IInstrument
    {
        private readonly Tier _tier = 0;
        public ItemType ObtainedType => ItemType.Everything;
        public override ItemType Type => ItemType.Instrument;
        public InventoryItem Mine(IItem item)
        {
            if (this._tier < item.Tier)
                return new InventoryItem(Storage.EmptyItem);

            return new InventoryItem(item, 1);
        }
    }
}
