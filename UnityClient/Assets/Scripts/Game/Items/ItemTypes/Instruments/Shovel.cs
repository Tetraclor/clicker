﻿using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "Shovel", menuName = "Instruments/Shovel")]
    public class Shovel : Instrument
    {
        public override ItemType ObtainedType => ItemType.Shovelable;
        public override ItemType Type => ItemType.Instrument;
    }
}
