﻿using Clicker.InventorySystem;
using Clicker.Items;
using Clicker.Locations;
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clicker.Items
{
    public abstract class Instrument : ScriptableItem,IInstrument
    {       
        
        private Enchantments _enchantments;
        private int _durability;
        public abstract ItemType ObtainedType { get; }

        public InventoryItem Mine(IItem item)
        {
            //Проверка, возможно ли текущим типом инструмента добыть этот ресурс - Это лежит на ресурс генератор.
            if (this.Tier < item.Tier)
                return null;

            var amount = 1;

            EnchatmentsApplyResult enchatmentsApplyResult = _enchantments.ApplyEnchantments();

            if(enchatmentsApplyResult.HasTheDurabilityDecreased)
                _durability--;
            if (enchatmentsApplyResult.HasTheDurabilityDecreased)
                _durability++;
            amount *= enchatmentsApplyResult.MultiplicationFactorOfLootingAndFortune;

            return new InventoryItem(item, amount);
        }
    }

    public class Enchantments
    {       
        //Todo: Можно валидировать сетеры. Реализовать эффективность.
        public int UnbreakingLevel { get; set; }
        public int MendingLevel { get; set; }
        public int FortuneLevel { get; set; }
        public int LootingLevel { get; set; }
        public int EfficientyLevel { get; set; }

        public Enchantments()
        {
            UnbreakingLevel = 0;
            MendingLevel = 0;
            FortuneLevel = 0;
            LootingLevel = 0;
            EfficientyLevel = 0;
        }

        public Enchantments(int unbreakingLevel, int mendingLevel, int fortuneLevel, int lootingLevel, int efficientyLevel)
        {
            UnbreakingLevel = unbreakingLevel;
            MendingLevel = mendingLevel;
            FortuneLevel = fortuneLevel;
            LootingLevel = lootingLevel;
            EfficientyLevel = efficientyLevel;
        }

        public EnchatmentsApplyResult ApplyEnchantments()
            => new EnchatmentsApplyResult(
                !GetUndreakingResult(),
                GetMendingResult(),
                GetMendingOrFortuneResult(),
                false
                );

        private bool GetUndreakingResult()
            => (Mathf.Pow(0.5f, UnbreakingLevel) <= UnityEngine.Random.value);

        private bool GetMendingResult()
            => (Mathf.Pow(0.5f, UnbreakingLevel) <= UnityEngine.Random.value);

        private int GetMendingOrFortuneResult()
            => UnityEngine.Random.Range(1, (FortuneLevel > LootingLevel ? FortuneLevel : LootingLevel) + 1);
        }
    }

    public class EnchatmentsApplyResult
    {
        public EnchatmentsApplyResult(bool hasTheDurabilityDecreased, bool hasTheDurabilityIncreased, int multiplicationFactorOfLootingAndFortune, bool hasTheEfficientyJustWorked)
        {
            HasTheDurabilityDecreased = hasTheDurabilityDecreased;
            HasTheDurabilityIncreased = hasTheDurabilityIncreased;
            MultiplicationFactorOfLootingAndFortune = multiplicationFactorOfLootingAndFortune;
            HasTheEfficientyJustWorked = hasTheEfficientyJustWorked;
        }

        public bool HasTheDurabilityDecreased { get; }
        public bool HasTheDurabilityIncreased { get; }
        public int MultiplicationFactorOfLootingAndFortune { get; }
        public bool HasTheEfficientyJustWorked { get; }
    }
