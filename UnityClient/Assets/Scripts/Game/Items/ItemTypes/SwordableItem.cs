using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "SwordableItem", menuName = "Items/SwordableItem")]
    public class SwordableItem : ScriptableItem
    {
        public override ItemType Type => ItemType.Swordable;
    }
}
