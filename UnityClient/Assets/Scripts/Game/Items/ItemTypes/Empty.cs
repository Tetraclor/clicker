﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "Empty", menuName = "Items/Empty")]
    public class Empty : ScriptableItem
    {
        public override ItemType Type => ItemType.Empty;
    }
}
