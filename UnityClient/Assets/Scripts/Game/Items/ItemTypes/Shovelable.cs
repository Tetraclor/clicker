﻿using UnityEngine;

namespace Clicker.Items
{
    [CreateAssetMenu(fileName = "ShovelableItem", menuName = "Items/ShovelableItem")]
    public class ShovelableItem : ScriptableItem
    {
        public override ItemType Type => ItemType.Shovelable;
    }
}