using UnityEngine;
using Newtonsoft.Json;

namespace Clicker.Items
{
    public interface IItem
    {       
        public Sprite Sprite { get;}
        public string Name { get; } 
        public string Description { get; }
        public ItemType Type { get; }
        public Tier Tier { get; }    
        public int Id { get; }
        public int Cost { get; }
    }
    
}
