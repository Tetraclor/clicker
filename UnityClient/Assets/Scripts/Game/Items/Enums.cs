﻿using UnityEngine;

namespace Clicker.Items
{
    public enum Tier
    {
        None,
        Wooden,
        Stone,
        Iron,
        Golden,
        Diamond,
        Netherite,
        //Bedrock
    }

    public enum EnchantmentType
    {
        Unbreaking,
        Mending,
        Fortune,
        Looting,
        Efficienty
    }

    public enum InstrumentType
    {
        Pickaxe,
        Sword,
        Axe,
        Shovel,
        //Hoe,
        //Shears,
        //FishingRod,
    }
    public enum ItemType {
        Instrument,
        Pickaxeable,
        Axeable,
        Shovelable,
        Swordable,
        Empty,
        Everything
    }
}
