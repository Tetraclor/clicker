﻿using Clicker.InventorySystem;

namespace Clicker.Items
{
    public interface IInstrument
    {
        public ItemType ObtainedType { get; }
        public InventoryItem Mine(IItem item);
    }
}
