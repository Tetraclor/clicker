using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ���������� �������
/// ������������� ������� ��������, � ���������� � �������.
/// ���������� �� �������. 
/// </summary>
namespace Danila
{
    public class ResourcesScrollView : MonoBehaviour
    {
        public ItemSlot ItemViewPrefab;
        public GameObject ContentPanel;

        Dictionary<string, ItemSlot> resourceNameToItemView = new Dictionary<string, ItemSlot>();

        public void CreateOrUpdateView(Resource resource)
        {
            if (resourceNameToItemView.TryGetValue(resource.Name, out ItemSlot itemView))
            {
                //itemView.Text.text = resource.ToString(); !!!��� ���� ������ ���������, ����� ����� ������!!!
            }
            else
            {
                CreateNewItemView(resource);
            }
        }

        void CreateNewItemView(Resource resource) // TODO ��������� ������ ��������������� ��������.
        {
            var item = Instantiate<ItemSlot>(ItemViewPrefab);
            item.transform.SetParent(ContentPanel.transform, false);
            //item.Text.text = resource.ToString(); !!!��� ���� ������ ���������, ����� ����� ������!!!
            resourceNameToItemView[resource.Name] = item;
        }
    }

}
