using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Clicker.Locations
{
    public class ResourceSeparator
    {
        ResourceContainer _allResources;
        Dictionary<ItemType, ResourceContainer> _groupedResources;
        public ResourceSeparator(List<Resource> resources)
        {
            _allResources = new ResourceContainer(resources);
            _groupedResources = resources.GroupBy(x => x.Item.Type).ToDictionary(key=>key.Key, collection=> new ResourceContainer(collection.ToList()));            
        }   
        public ResourceContainer GetResourceContrainerByItemType(ItemType itemType)
        {
            if (itemType == ItemType.Everything)
                return _allResources;

            if (_groupedResources.ContainsKey(itemType))
                return _groupedResources[itemType];
            else
                return null;
        }
    }
    public class ResourceContainer
    {        
        public ResourceContainer(List<Resource> resources)
        {
            CloneResources(resources);
            NormalizeAndSetLadderProbabilities();
        }
        private List<Resource> _resources = new List<Resource>();

        protected double ladderHighProbabilityValue = 0f;        
        public List<Resource> Resources { get => _resources;}
        private void CloneResources(List<Resource> resources)
        {
            foreach (var resource in resources)
            {
                _resources.Add(resource.Clone() as Resource);
            }
        }
        private void NormalizeAndSetLadderProbabilities()
        {            
            double normalizedCoefficient = _resources.Sum(x=>x.Probability);            
            foreach (var resource in _resources)
            {
                var ladderProbability = ladderHighProbabilityValue;
                ladderHighProbabilityValue += resource.Probability / normalizedCoefficient;                
                resource.Probability = ladderProbability;                             
            }
        }
    }
}
