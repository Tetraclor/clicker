using Clicker.InventorySystem;
using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.Locations
{
    public class ResourceGenerator
    {        
        private ResourceSeparator _resourceSeparator;
        private IItem _empty;
        public ResourceGenerator(List<Resource> resources)
        {            
            _resourceSeparator = new ResourceSeparator(resources);
            _empty = Storage.EmptyItem;
        }       
        
        public IItem Get(ItemType type)
        {
            return GetByRandomValue(Random.value,type);
        }
        public IItem GetByRandomValue(float randomValue, ItemType type)
        {
            
            ResourceContainer resourceContrainer = _resourceSeparator.GetResourceContrainerByItemType(type);

            if (resourceContrainer != null)
            {               
                ScriptableItem item = null;
                Debug.Log(randomValue);
                for (int i = 0; i < resourceContrainer.Resources.Count; i++)
                {
                    if (resourceContrainer.Resources.Count > 1)
                    {
                        if(i == resourceContrainer.Resources.Count -1)
                        {                            
                             item = resourceContrainer.Resources[i].Item;                            
                        }
                        else
                        {
                            if(resourceContrainer.Resources[i].Probability <= randomValue && resourceContrainer.Resources[i+1].Probability >=randomValue)
                            {
                                item = resourceContrainer.Resources[i].Item;
                                break;
                            }
                        }
                    }
                    else
                    {
                        item = resourceContrainer.Resources[i].Item;
                    }
                }
                if (item == null)
                    throw new System.NullReferenceException("Item is null");
                return item;
            }
            return _empty;
        }
    }
}
