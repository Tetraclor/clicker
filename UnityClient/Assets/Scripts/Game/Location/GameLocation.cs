using Clicker.InventorySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.Locations
{
    public class GameLocation : Location
    {
        [SerializeField]
        ParticleSystem iconSystem;
        [SerializeField]
        ParticleSystem effect;
        public override void GenerateResource()
        {            
            var generatedResource = _resourceGenerator.Get(_inventory.Equipment.Instrument.ObtainedType);
            var obtainedItem = _inventory.Equipment.Instrument.Mine(generatedResource);
            AddParticleEffect(obtainedItem.Sprite);
            _inventory.AddItem(obtainedItem);
        }
        public void AddParticleEffect(Sprite iconSprite)
        {
            Vector3 globalMousePos;
            
            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(Canvas, Input.mousePosition, cam, out globalMousePos))
            {
                var eff = GameObject.Instantiate(effect);
                var icon = GameObject.Instantiate(iconSystem);
                icon.transform.position = new Vector3(globalMousePos.x,globalMousePos.y,89.90f);
                eff.transform.position = globalMousePos;                
                icon.textureSheetAnimation.AddSprite(iconSprite);
                icon.Play();
                eff.Play();
            }
        }
    }
}
