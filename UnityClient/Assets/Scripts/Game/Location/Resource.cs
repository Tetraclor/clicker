using Clicker.Items;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.Locations
{
    [Serializable]
    public class Resource :IComparable<Resource>,ICloneable
    {
        [SerializeField]
        private double _probability;

        public ScriptableItem Item;
        
        public double Probability
        {
            get => _probability;
            set
            {
                if (value > 1)
                {                   
                    throw new System.ArgumentException("Probability shouldn't be more than 1f");
                }
                else
                {
                    _probability = value;
                }
            }
        }
        public int CompareTo(Resource other)
        {
            if (Probability > other.Probability)
                return 1;
            else if (Probability < other.Probability)
                return -1;
            else return 0;
        }

        public object Clone()
        {
            return new Resource { Item = this.Item, Probability = _probability };
        }
    }
}
