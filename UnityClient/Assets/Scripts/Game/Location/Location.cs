using Clicker.InventorySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Clicker.Locations
{
    public abstract class Location : MonoBehaviour
    {
        [SerializeField]
        protected Inventory _inventory;
        [SerializeField]
        private List<Resource> _resources;
        protected RectTransform Canvas;
        protected Camera cam;

        protected ResourceGenerator _resourceGenerator;

        private void Awake()
        {            
            _resourceGenerator = new ResourceGenerator(_resources);
            Canvas =GetComponent<RectTransform>();
            cam = Camera.main;
        }
        public abstract void GenerateResource();
    }
}
