﻿
using Clicker.Locations;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Randomizer<T>
{
    readonly Dictionary<T, float> _chances = new Dictionary<T, float>();

    List<T> _items = new List<T>();
    List<float> _chancesLadder = new List<float>();

    public Randomizer(Dictionary<T, float> chances)
    {
        _chances = chances.ToDictionary(v => v.Key, v => v.Value);

        Init();
    }
    void Init()
    {
        var pairs = this._chances.ToList();

        pairs.Sort((a, b) => (int)(a.Value - b.Value) * 100000);

        _items = pairs.Select(v => v.Key).ToList();
        var chances = pairs.Select(v => v.Value).ToList();

        if (chances.Sum() != 1)
        {
            // Должно быть в сумме 1. 
        }

        _chancesLadder = new List<float>();
        var sum = 0f;
        foreach (var chance in chances)
        {
            sum += chance;
            _chancesLadder.Add(sum);
        }
        ;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="rndValue">Ожидается значение от 0 до 1</param>
    /// <returns></returns>
    public T Get(float rndValue)
    {
        var index = _chancesLadder.FindIndex(v => v > rndValue);
        return _items[index];
    }

    public T GetUseUnityRandom()
    {
        return Get(Random.Range(0f, 1f));
    }
}
