using Clicker.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Clicker.InventorySystem;

namespace Clicker.Craft
{
    [CreateAssetMenu(fileName = "Recipe", menuName = "Recipe")]
    public class CraftRecipe : ScriptableObject, IItem
    {        
        public List<Piece> _pieces;
        
        public ScriptableItem _targetItem;

        public Sprite Sprite => _targetItem.Sprite;

        public string Name => _targetItem.Name;

        public string Description => _targetItem.Description;

        public ItemType Type => _targetItem.Type;

        public Tier Tier => _targetItem.Tier;

        public int Id => _targetItem.Id;

        public int Cost => _targetItem.Cost;
    }
    [Serializable]
    public class Piece:IItem,ICountable
    {
        public  ScriptableItem _item;
        public Sprite Sprite => _item.Sprite;

        public string Name => _item.Name;

        public string Description => _item.Description;

        public ItemType Type => _item.Type;

        public Tier Tier => _item.Tier;

        public int Id => _item.Id;

        public int Cost => _item.Cost;

        public int Count => count;

        public int count;
    }
}

