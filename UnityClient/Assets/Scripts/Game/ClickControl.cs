using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ClickControl : MonoBehaviour
{
    public RestClientImpl RestClientImpl;
    public TextMeshProUGUI ClicksCountText;
    private int DeltaClicks;
    private int AllClicksFromServer;

    void Start()
    {
        InvokeRepeating(nameof(ServerSync), 0, 3);
    }

    public void Click()
    {
        DeltaClicks++;
        UpdateClickCount(DeltaClicks + AllClicksFromServer);
    }

    public void UpdateClickCount(int clicks)
    {
        ClicksCountText.text = $"���������: {clicks}";
    }

    public void Success(string allClicks)
    {
        AllClicksFromServer = int.Parse(allClicks);
        UpdateClickCount(AllClicksFromServer);
    }

    public void ServerSync()
    {
        Debug.Log("������������� � ��������");
        RestClientImpl.AddClick(DeltaClicks, Success);
        DeltaClicks = 0;
    }
}
