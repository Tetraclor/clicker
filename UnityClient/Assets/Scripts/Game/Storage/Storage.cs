using Clicker.Items;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Clicker.InventorySystem
{
    [CreateAssetMenu(fileName = "Storage", menuName = "Storage")]
    public class Storage : ScriptableObject
    {       
        private static Hand _hand;
        private static Empty _emptyItem;

        private Dictionary<int, IItem> _items =  new Dictionary<int, IItem>();
        public static Hand Hand { get => _hand;}
        public static Empty EmptyItem { get => _emptyItem;}        

        public void LoadAllItems()
        {
            LoadSpecialItems();
            var loadedObjects = Resources.LoadAll<ScriptableItem>(Project.GameObjects);
            var loadedInstrumets = Resources.LoadAll<Instrument>(Project.Instuments);
            foreach (var item in loadedObjects)
            {
                _items.Add(item.Id, item);
            }
            foreach (var item in loadedInstrumets)
            {
                _items.Add(item.Id, item);
            }

        }
        private void LoadSpecialItems()
        {
            _hand = Resources.Load<Hand>(Project.SpecialObject.Hand);
            _emptyItem = Resources.Load<Empty>(Project.SpecialObject.Empty);
        }
        public IItem GetItem(int id)
        {
            if (_items.ContainsKey(id) == false)
                throw new KeyNotFoundException($"Storage doesn't contain id:{id}");
            return _items[id];
        }
        
    }
}
