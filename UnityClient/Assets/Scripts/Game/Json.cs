﻿using Clicker.InventorySystem;
using Newtonsoft.Json;

namespace Clicker.Json
{
    public class JsonItemObject
    {
        [JsonProperty]
        public int Count { get; set; }
        [JsonProperty]
        public int Id { get; set; }
    }
    
}
