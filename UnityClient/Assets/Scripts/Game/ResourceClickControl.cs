﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;


namespace Danila
{
    public class Resource
    {
        public string Name;
        public long Count;
    }
    public class ResourceStorage
    {
        public Dictionary<string, Resource> ResourcesDict = new Dictionary<string, Resource>();

        public ResourceStorage(string json)
        {
            // Типа загрузка из файла
            ResourcesDict["Дерево"] = new Resource() { Name = "Дерево", Count = 10 };
            ResourcesDict["Яблоко"] = new Resource() { Name = "Яблоко", Count = 10 };
        }

        public void AddResource(Resource resource)
        {
            ResourcesDict[resource.Name].Count += resource.Count;
        }

        public void Save()
        {
            // Сохраняем в файл.
        }
    }

    public class ResourceClickControl : MonoBehaviour
    {
        public ResourcesScrollView ResourcesScrollView;

        private ResourceStorage ResourceStorage = new ResourceStorage("");
        private Randomizer<Resource> Randomizer;

        void Start()
        {
            RandomizerInit();
        }

        void RandomizerInit()
        {
            var chances = new Dictionary<Resource, float>();

            var resourcesCountInLocations = 2;

            foreach (var item in ResourceStorage.ResourcesDict.Values)
            {
                chances[item] = 1f / resourcesCountInLocations; //Сделали шанс выпадения одинаковым для всех ресурсов пока что
            }
            Randomizer = new Randomizer<Resource>(chances);
        }

        public void Click()
        {
            var rndValue = Random.Range(0f, 1f);
            var resource = Randomizer.Get(rndValue);

            resource.Count++; // Так как сылка, то норм.

            // Отображаем только полное значение
            ResourcesScrollView.CreateOrUpdateView(new Resource() { Name = resource.Name, Count = resource.Count });
        }
    }


    public class ResourceClickControlServerSync : MonoBehaviour
    {
        public RestClientImpl RestClientImpl;
        public TextMeshProUGUI ClicksCountText;
        public ResourcesScrollView ResourcesScrollView;

        private UserGameInfo UgiAll;
        private readonly UserGameInfo UgiDelta = new UserGameInfo();
        private bool Changed = false;
        private Randomizer<ResourceCount> RandomizerForDelta;
        private Randomizer<ResourceCount> RandomizerForAll;

        private Dictionary<int, string> idToNameResource = new Dictionary<int, string>()
        {
            [1] = "Дерево",
            [2] = "Яблоко",
        };

        void Start()
        {
            DeltaUgiClear();
            InvokeRepeating(nameof(ServerSync), 0, 1);

            RestClientImpl.LogIn("test", "test", v => RestClientImpl.Get<UserGameInfo>(Success));
        }

        void RandomizerInit()
        {
            var chancesForDelta = new Dictionary<ResourceCount, float>();
            var chancesForAll = new Dictionary<ResourceCount, float>();

            var resourcesCountInLocations = 2;

            foreach (var item in UgiDelta.mineResources)
            {
                chancesForDelta[item] = 1f / resourcesCountInLocations; //Сделали шанс выпадения одинаковым для всех ресурсов пока что
            }

            foreach (var item in UgiAll.mineResources)
            {
                chancesForAll[item] = 1f / resourcesCountInLocations;
            }

            RandomizerForDelta = new Randomizer<ResourceCount>(chancesForDelta);
            RandomizerForAll = new Randomizer<ResourceCount>(chancesForAll);
        }

        public void DeltaUgiClear()
        {
            UgiDelta.countClicks = 0;
            for (int i = 0; i < UgiDelta.mineResources.Length; i++)
            {
                UgiDelta.mineResources[i].count = 0;
            }
        }

        public void Click()
        {
            Changed = true;

            var rndValue = Random.Range(0f, 1f);
            var deltaResource = RandomizerForDelta.Get(rndValue); // Костыль, чтобы обновлять значение как дельты, которая отправляется на сервер,
            var allResource = RandomizerForAll.Get(rndValue); // Так и полное значение

            deltaResource.count++;
            allResource.count++; // Так как сылка, то норм.

            // Отображаем только полное значение
            ResourcesScrollView.CreateOrUpdateView(new Resource() { Name = idToNameResource[allResource.resourceId], Count = allResource.count });
        }

        public void Success(UserGameInfo ugi)
        {
            Debug.Log(ugi.ToString());

            UgiAll = ugi;

            foreach (var item in ugi.mineResources)
            {
                ResourcesScrollView.CreateOrUpdateView(new Resource() { Name = idToNameResource[item.resourceId], Count = item.count });
            }

            UgiDelta.mineResources = UgiAll
                .mineResources
                .Select(v => new ResourceCount() { resourceId = v.resourceId })
                .ToArray();

            DeltaUgiClear();
            RandomizerInit();
        }

        public void ServerSync()
        {
            if (Changed == false) return;
            Debug.Log("Синхронизация с сервером 2");
            Changed = false;
            RestClientImpl.Post(UgiDelta);
            DeltaUgiClear();
        }
    }
}