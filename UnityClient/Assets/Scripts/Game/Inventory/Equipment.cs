﻿using Clicker.Items;
using UnityEngine;

namespace Clicker.InventorySystem
{
    public class Equipment
    {
        private IInstrument _defaultInstrument;
        private IInstrument _currentInstrument;
        public Equipment()
        {
            _defaultInstrument = Resources.Load<Hand>(Project.SpecialObject.Hand);
            _currentInstrument = _defaultInstrument;
        }
        public IInstrument Instrument { get => _currentInstrument; }
        public void EquipInstrument(IInstrument _instrument)
        {
            _currentInstrument = _instrument;
        }

        public void TakeOffInstrument()
        {
            _currentInstrument = _defaultInstrument;
        }
    }
}
