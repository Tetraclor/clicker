using Clicker.Items;
using Newtonsoft.Json;
using System;
using UnityEngine;

namespace Clicker.InventorySystem
{
    public class InventoryItem : IItem, ICountable
    {
        private IItem _item;
        private int _count;

        #region properties
        public Sprite Sprite => _item.Sprite;
        public string Name => _item.Name;
        public string Description => _item.Description;
        public ItemType Type => _item.Type;      
        public Tier Tier => _item.Tier;
        public int Count => _count;
        public int Id => _item.Id;
        public int Cost => _item.Cost;
        #endregion
                
        public InventoryItem(IItem item, int count=1)
        {
            _item = item;
            _count = count;
        }
        public void Add()
        {
            _count++;
        }
        public void AddAmount(int amount)
        {
            if(_count + amount>=0)
            {
                _count += amount;
            }
            else
            {
                throw new ArgumentException("Sum below then zero.");
            }
        }
    }
}