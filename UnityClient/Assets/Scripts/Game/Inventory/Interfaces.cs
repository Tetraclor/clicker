namespace Clicker.InventorySystem
{
    public interface ICountable
    {
        public int Count { get; }
    }
    public interface ILoadable
    {
        void Load();
    }
    public interface ISaveable
    {
        void Save();
    }

}
