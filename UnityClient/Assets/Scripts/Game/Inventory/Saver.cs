﻿using Clicker.Json;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Clicker.InventorySystem
{
    public class Saver : ISaveable
    {      
        private TextAsset _data;
        private TextAsset _emeraldsData;
        private Inventory _inventory;
        public Saver(TextAsset data, TextAsset emeraldsData, Inventory inventory)
        {
            _data = data;
            _inventory = inventory;
        }
        public void Save()
        {
            List<JsonItemObject> packedItems = new List<JsonItemObject>();
            foreach(var unpackedItem in _inventory.Items.Values)
            {
                packedItems.Add(ConvertToInventoryJsonItem(unpackedItem));
            }
            var savedData = JsonConvert.SerializeObject(packedItems);
            //TODO оптимизировать сохранение файла
            File.WriteAllText(@"Assets/Resources/Player/InventoryItems.txt", savedData);
            File.WriteAllText(@"Assets/Resources/Player/Emeralds.txt", _inventory.Emeralds.ToString());
            //using (FileStream fstream = new FileStream(@"Assets/Resources/Player/InventoryItems.txt", FileMode.Create))
            //{                
            //    using (StreamWriter sWriter = new StreamWriter(fstream))
            //    {
            //        sWriter.Write(savedData);
            //    }                
            //}
            //using (FileStream fstream = new FileStream(@"Assets/Resources/Player/Emeralds.txt", FileMode.Create))
            //{
            //    using (StreamWriter sWriter = new StreamWriter(fstream))
            //    {
            //        sWriter.Write(_inventory.Emeralds.ToString());
            //    }
            //}
        }
        public JsonItemObject ConvertToInventoryJsonItem(InventoryItem inventoryItem)
        {
            return new JsonItemObject { Id = inventoryItem.Id, Count = inventoryItem.Count };
        }
    }
}
