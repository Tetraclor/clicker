using Clicker.Items;
using Clicker.Json;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Clicker.InventorySystem
{
    [CreateAssetMenu(fileName = "Inventory", menuName = "Inventory")]
    public class Inventory : ScriptableObject
    {
        public Equipment Equipment;        
        private Dictionary<int, InventoryItem> _items;                   
        public Dictionary<int, InventoryItem> Items { get => _items;}

        public static event Action<int> ChangeEmeraldsValueEvent;
        int _emeralds =0;
        public int Emeralds { get => _emeralds; set {
                _emeralds = value;
                ChangeEmeraldsValueEvent?.Invoke(_emeralds);
            } }

        public void OnEnable()
        {
            _items = new Dictionary<int, InventoryItem>();
            Equipment = new Equipment();
        }
        public void AddItem(IItem item)
        {
            if (item != null)
            {
                if (Items.ContainsKey(item.Id))
                {
                    Items[item.Id].Add();
                }
                else
                {
                    Items.Add(item.Id, new InventoryItem(item));
                }
            }
        }
        public void AddItem(InventoryItem item)
        {
            if (Items.ContainsKey(item.Id))
            {
                Items[item.Id].AddAmount(item.Count);
            }
            else
            {
                Items.Add(item.Id, item);
            }
        }   
        public void RemoveItem(IItem item, int count = 1)
        {
            if (Items.ContainsKey(item.Id))
            {
                Items[item.Id].AddAmount(-count);
                if(Items[item.Id].Count <= 0)
                {
                    Items.Remove(item.Id);
                }
            }
        }
        public void RemoveAllItemsOfCertanType(IItem item)
        {
            if (Items.ContainsKey(item.Id))
            {                
                Items.Remove(item.Id);                
            }
        }
    }
}
