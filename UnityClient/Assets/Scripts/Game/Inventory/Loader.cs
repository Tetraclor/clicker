﻿using Clicker.Json;
using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Clicker.InventorySystem
{
    public class Loader : ILoadable
    {
        private TextAsset _data;
        private TextAsset _emeraldsData;
        private Storage _storage;
        private Inventory _inventory;
        public Loader(TextAsset data, TextAsset emeraldsData, Inventory inventory, Storage storage)
        {
            _data = data;
            _inventory = inventory;
            _storage = storage;
            _emeraldsData = emeraldsData;
        }
        public void Load()
        {
            List<JsonItemObject> _packedItems = JsonConvert.DeserializeObject<List<JsonItemObject>>(_data.text);
            if (_packedItems != null)
            {
                foreach (var packedItem in _packedItems)
                {
                    _inventory.AddItem(ConvertToInventoryItem(packedItem));
                }
            }
            _inventory.Emeralds = int.Parse(_emeraldsData.text);
        }
        public InventoryItem ConvertToInventoryItem(JsonItemObject jsonItem)
        {
            var storageItem = _storage.GetItem(jsonItem.Id);
            return new InventoryItem(storageItem, jsonItem.Count);
        }
    }
}
 