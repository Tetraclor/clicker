using Clicker.InventorySystem;
using UnityEngine;

namespace Clicker.SaveAndLoad
{
    public class SaveAndLoadSystem : MonoBehaviour
    {
        private ILoadable _loader;
        private ISaveable _saver;
        private TextAsset _inventoryData;
        private TextAsset _emeraldsData;

        private Inventory _inventory;
        private Storage _storage;
        private void Awake()
        {
            _inventory = Resources.Load<Inventory>(Project.InventoryScriptableObject);
            _emeraldsData = Resources.Load<TextAsset>(Project.Emeralds);
            _inventoryData = Resources.Load<TextAsset>(Project.SaveInventoryFile);
            _storage = Resources.Load<Storage>(Project.StorageScriptableObject);
            _loader = new Loader(_inventoryData, _emeraldsData, _inventory, _storage);
            _saver = new Saver(_inventoryData, _emeraldsData, _inventory);
            LoadOnSetup();
        }
        
        public void LoadOnSetup()
        {
            LoadStorageData();
            LoadInventoryData();
        }
        public void LoadStorageData()
        {
            _storage.LoadAllItems();
        }
        public void LoadInventoryData()
        {
            _loader.Load();
        }

        public void SaveInventoryData()
        {
            _saver.Save();
        }
    }
}
