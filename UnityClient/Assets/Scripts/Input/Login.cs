using TMPro;
using UnityEngine;

namespace Clicker.InputData
{
    [RequireComponent(typeof(TMP_InputField))]
    public class Login : MonoBehaviour, IAuthenticationInputData
    {
        private TMP_InputField _inputField;

        private void Awake()
        {
            _inputField = GetComponent<TMP_InputField>();
        }

        public string GetInputValue()
        {
            return _inputField.text;
        }

        public Validation Validate()
        {
            if (string.IsNullOrEmpty(GetInputValue()))
            {
                return new EmptyField();
            }
            else
                return new Correct();
        }
        public void ClearInputValue()
        {
            _inputField.text = string.Empty;
        }
    }
}
