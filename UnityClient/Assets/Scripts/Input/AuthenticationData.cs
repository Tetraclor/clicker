using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.InputData
{
    public interface IAuthenticationInputData
    {
        string GetInputValue();
        Validation Validate();
        void ClearInputValue();
    }
}
