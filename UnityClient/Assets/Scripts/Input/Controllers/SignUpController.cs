using Clicker.Notification;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Clicker.InputData
{
    public class SignUpController : AuthenticationController
    {
        [SerializeField]
        private RestClientImpl _restClient;
        private IAuthenticationInputData Login => _inputs[0];
        private IAuthenticationInputData Password => _inputs[1];
        private IAuthenticationInputData ReferalKey => _inputs[3];

        public override void Send()
        {
            if (Validate())
            {
                _restClient.Register(
                    Login.GetInputValue(),
                    Password.GetInputValue(),
                    ReferalKey.GetInputValue(),
                    Succes,
                    Fail
                );
            }
        }

        public void Succes(string token)
        {
            SceneManager.LoadScene("Game"); // �� ����, ��������� ��������� ���������� ����� �����. �� ���� ���. 
            Debug.Log("�������� �����������. �����: " + token);
        }

        public void Fail(string error)
        {
            NotificationPanel.Instance.ShowMessage(error);
        }

        protected override bool Validate()
        {
            foreach (var input in _inputs)
            {
                var validation = input.Validate();
                if (validation.IsCorrect == false)
                {
                    NotificationPanel.Instance.ShowMessage(validation.Message);
                    return false;
                }
            }
            return true;
        }
    }
}
