using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.InputData
{
    public abstract class AuthenticationController : MonoBehaviour
    {
        protected IAuthenticationInputData[] _inputs;

        private void Awake()
        {
            _inputs = GetComponentsInChildren<IAuthenticationInputData>();
        }
        public abstract void Send();
        protected abstract bool Validate();
    }
}
