using Clicker.Notification;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Clicker.InputData
{
    public class SignInController : AuthenticationController
    {
        [SerializeField]
        private RestClientImpl _restClient;
        private IAuthenticationInputData Login => _inputs[0];
        private IAuthenticationInputData Password => _inputs[1];
        
        public override void Send()
        {
            if (Validate())
            {
                _restClient.LogIn(
                    Login.GetInputValue(), 
                    Password.GetInputValue(),
                    Success, 
                    Fail
                );
            }
        }

        public void Success(string token)
        {
            SceneManager.LoadScene("Game2"); // �� ����, ��������� ��������� ���������� ����� �����. �� ���� ���. 
            Debug.Log("�������� ����, �����: " + token);
        }

        public void Fail(string message)
        {
            NotificationPanel.Instance.ShowMessage(message);
        }
        
        protected override bool Validate()
        {
            foreach(var input in _inputs)
            {
                var validation = input.Validate();
                if (validation.IsCorrect == false)
                {
                    NotificationPanel.Instance.ShowMessage(validation.Message);
                    return false;
                }
            }
            return true;
        }
    }
}
