using TMPro;
using UnityEngine;

namespace Clicker.InputData
{
    public class ReferralCode : MonoBehaviour, IAuthenticationInputData
    {
        private TMP_InputField _inputField;
        private void Awake()
        {
            _inputField = GetComponent<TMP_InputField>();
        }
        public string GetInputValue()
        {
            return _inputField.text;
        }
        public Validation Validate()
        {
            var value = GetInputValue();
            if (value.Length == 0) // ���� ���� ������, ������ ��� ������������ ����
                return new Correct();
            else if (value.Length == 6) // ����� ������������ ���� ������ ����� �����, ����� �� ������ ���� ��������� ��������.
                return new Correct();
            else
                return new Incorrect("����� ������������ ���� ������ ���� ����� ����� ��������");
        }

        public void ClearInputValue()
        {
            _inputField.text = string.Empty;
        }
    }
}
