using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.InputData
{
    public class FieldClearer : MonoBehaviour
    {
        protected IAuthenticationInputData[] _inputs;

        private void Awake()
        {
            _inputs = GetComponentsInChildren<IAuthenticationInputData>();
        }
        public void Clear()
        {
            foreach(var input in _inputs)
            {
                input.ClearInputValue();
            }
        }
    }
}