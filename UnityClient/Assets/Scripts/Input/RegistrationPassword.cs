using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.InputData
{
    public class RegistrationPassword : MonoBehaviour, IAuthenticationInputData
    {
        [SerializeField]
        private Password _password;
        [SerializeField]
        private Password _confirmPassword;

        public string GetInputValue()
        {
            if (_password.GetInputValue() != _confirmPassword.GetInputValue())
            {
                throw new UnityException("They aren't equal.");
            }
            else
                return _password.GetInputValue();
        }

        public Validation Validate()
        {
            if (string.IsNullOrEmpty(_password.GetInputValue()) || string.IsNullOrEmpty(_confirmPassword.GetInputValue()))
            {
                return new EmptyField();
            }
            if(_password.GetInputValue() != _confirmPassword.GetInputValue())
            {
                return new PasswordsAreNotEqual();
            }
            else
                return new Correct();
        }
        public void ClearInputValue()
        {
            _password.ClearInputValue();
            _confirmPassword.ClearInputValue();
        }
    }
}
