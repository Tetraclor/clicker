using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.InputData
{
    public abstract class Validation
    {        
        public abstract bool IsCorrect { get;}

        public abstract string Message { get; }
    }

    public class Incorrect : Validation
    {
        private readonly string message;

        public Incorrect(string message)
        {
            this.message = message;
        }

        public override bool IsCorrect => false;

        public override string Message => message;
    }
}
