using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clicker.InputData
{
    public class Correct : Validation
    {
        public override string Message => "Everything is fine";
        public override bool IsCorrect => true;

    }
}
