using NUnit.Framework;
using Moq;
using Clicker.Items;
using Clicker.Locations;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Tests
{
    public class ResourceContainerTest
    {
        private ScriptableItem _stubItem;
        private ResourceContainerStub _stubContainer;
        private List<Resource> _resources;

        #region Setup
        private void SetupMethod(int memberCount)
        {      
            SetupItemStub();
            CreateResourcesWithMemberCount(memberCount);
            _stubContainer = new ResourceContainerStub(_resources);
        }
        private void SetupItemStub()
        {
            _stubItem = ScriptableObject.CreateInstance("Empty") as ScriptableItem;          
        }
        private void CreateResourcesWithMemberCount(int count)
        {
            _resources = new List<Resource>();
            for (int i = 0; i < count; i++)
            {
                _resources.Add(new Resource() { Item = _stubItem, Probability = UnityEngine.Random.value });
            }
        }
        #endregion
        #region Stubs
        private class ResourceContainerStub : ResourceContainer
        {
            public ResourceContainerStub(List<Resource> resources) : base(resources)
            {

            }
            public double LadderHighProbabilityValue { get => ladderHighProbabilityValue; }
        }
        #endregion

        [Test]           
        [TestCase(1)]
        public void ResourceContainer_CheckSumProbability_EqualsOne([Random(1, 150, 20)] int resourceCount)
        {
            SetupMethod(resourceCount);
            Assert.AreEqual(1f,Math.Round(_stubContainer.LadderHighProbabilityValue,9));
        }     

    }
}
