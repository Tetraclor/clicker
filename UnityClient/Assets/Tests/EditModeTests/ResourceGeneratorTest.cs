using System.Collections.Generic;
using Clicker;
using Clicker.InventorySystem;
using Clicker.Items;
using Clicker.Locations;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ResourceGeneratorTest
    {
        private List<Resource> _resources;
        private ResourceGenerator _resourceGenerator;
        private Storage _storage;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _storage = Resources.Load<Storage>(Project.StorageScriptableObject);
            _storage.LoadAllItems();
        }
        [SetUp]
        public void Setup()
        {
            _resources = new List<Resource>();
            _resourceGenerator = new ResourceGenerator(_resources);
        }

        [Test]
        public void Get_EmptyItem_ItemTypeIsEqualsEmpty()
        {
            IItem emptyItem = _resourceGenerator.Get(ItemType.Empty);
            Assert.AreEqual(ItemType.Empty, emptyItem.Type);
        }
    }
}