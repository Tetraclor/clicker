using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParticleEffect : MonoBehaviour
{
    private Image image;
    private float distance = 20f;
    private RectTransform rectTransform;
    public void SetImage(Sprite sprite)
    {
        rectTransform = GetComponent<RectTransform>();
        image.GetComponent<Image>();
        image.sprite = sprite;        
    }
   
}
